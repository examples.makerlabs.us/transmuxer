# specify the VCL syntax version to use
vcl 4.1;

# import vmod_dynamic for better backend name resolution
import dynamic;

# we won't use any static backend, but Varnish still need a default one
backend default none;

# {
#  .host = "www";
#  .port = "8080";
#}

# set up a dynamic director
# for more info, see https://github.com/nigoroll/libvmod-dynamic/blob/master/src/vmod_dynamic.vcc
sub vcl_init {
  new d = dynamic.director(port = "8080");
}

sub vcl_recv {
  if (req.http.User-Agent == "healthchecker" && req.url == "/healthcheck") {
    return (synth(800));
  }
  set req.http.host = "www";
  set req.backend_hint = d.backend("www");
}

sub vcl_synth {
  if (resp.status == 800) {
    set resp.status = 200;
    set resp.reason = "OK";
    synthetic("Success");
    return(deliver);
  }
}

sub vcl_backend_response {
  if (beresp.status == 200) {
    if (bereq.url ~ "(\.m3u8|.hls|.ts|.mp4|.mp3)$") {
     set beresp.ttl = 2s;
     set beresp.http.cache-control = "public, max-age=2";
    }
  } else if (beresp.status > 399) {
     set beresp.ttl = 1s;
  }
}


sub vcl_deliver {
  if (resp.status >= 500 && req.restarts < 2) {
    return(restart);
  }
}