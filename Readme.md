
# Simple example of doing transcoding over rtmp and serving it via varnish cache / nginx.

## Dependencies
* Docker 19+
* Docker Compose
* Web browser with HLS plugins installed
* An rtmp streaming software (OBS or otherwise) to push to the rtmp host

## To launch:
* Clone this repo
* cd to the root of the repo
* docker-compose up

## To use:
* Setup an RTMP feed directed at rtmp://localhost:1935/live/stream

## To watch:
* Browse to  http://localhost:8080/index.m3u8

It takes ~1 minute for the rtmp / hls cache to fill & begin writing segements.

## Customization:
  Updates to the ffmpeq arguments can change the hls formats, 